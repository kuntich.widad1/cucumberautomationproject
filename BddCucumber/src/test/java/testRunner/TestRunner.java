package testRunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features={"features"},//chemin du dossier contenant les features
		glue={"steps"}, //chemin du dossier contenant les steps definitions 
		dryRun = false, //treu : chrche les features qui n'ont pas de glue code et les runner
		monochrome = true,//formatter l'afficahge dans la console
		tags="@sc1",
		//tags="@tag1 or @tag2", //selectionner les senarios a executer
		//name = "Title",//cherche et execute les scenarios qui contienne l'expression 
		plugin ={"pretty","json:target/json-report/cucumber.json"}
		
		)
public class TestRunner {

}
