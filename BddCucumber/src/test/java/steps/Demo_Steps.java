package steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Demo_Steps {

	@Given("j ouvrir le navigateur")
	public void j_ouvrir_le_navigateur() {
	   System.out.println("les etapes 1");
	}

	@Given("je suis dans la page Login")
	public void je_suis_dans_la_page_Login() {
		System.out.println("les etapes 2");
	}

	@When("lorsque je saisis l identifiant")
	public void lorsque_je_saisis_l_identifiant() {
		System.out.println("les etapes 3");
	}

	@When("je saisis le mot de passe")
	public void je_saisis_le_mot_de_passe() {
		System.out.println("les etapes 4");
	}

	@When("je clique sur le button LOGIN")
	public void je_clique_sur_le_button_LOGIN() {
		System.out.println("les etapes 5");
	}

	@Then("je vais etre dirige a la home page")
	public void je_vais_etre_dirige_a_la_home_page() {
		System.out.println("les etapes 6");
	}

	@Then("je valide que le site est affiche")
	public void je_valide_que_le_site_est_affiche() {
		System.out.println("les etapes 7");
	}
}
